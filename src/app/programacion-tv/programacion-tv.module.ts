import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProgramacionTvPageRoutingModule } from './programacion-tv-routing.module';

import { ProgramacionTvPage } from './programacion-tv.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProgramacionTvPageRoutingModule
  ],
  declarations: [ProgramacionTvPage]
})
export class ProgramacionTvPageModule {}
