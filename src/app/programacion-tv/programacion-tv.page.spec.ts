import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProgramacionTvPage } from './programacion-tv.page';

describe('ProgramacionTvPage', () => {
  let component: ProgramacionTvPage;
  let fixture: ComponentFixture<ProgramacionTvPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramacionTvPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProgramacionTvPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
