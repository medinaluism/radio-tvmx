import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProgramacionTvPage } from './programacion-tv.page';

const routes: Routes = [
  {
    path: '',
    component: ProgramacionTvPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProgramacionTvPageRoutingModule {}
