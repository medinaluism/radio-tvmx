import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MenuController, NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-tv-vivo',
  templateUrl: './tv-vivo.page.html',
  styleUrls: ['./tv-vivo.page.scss'],
})
export class TvVivoPage implements OnInit {
  @Input() texto = 'Cargando...';
  loader = false;
  ruta_video;
  datos_video;
  tele_vivo;
  tele;
  constructor(private route: Router, private http: HttpClient,
    private navCtrl: NavController, private menu: MenuController, private dom: DomSanitizer) { //, private statusBar: StatusBar
    }

  ngOnInit() {
    this.menu.enable(false);
    this.tv_Vivo();
  }

  tv_Vivo(){
    this.loader = true;
    const dir = 'https://hyperiontechlab.com:9016/livetvapp/getStreamingTvApp';
    return this.http.get(dir)
     .subscribe((solicitud)=>{
      this.loader = false;
       this.datos_video= solicitud;
       this.tele_vivo = this.datos_video.data[0];
       this.ruta_video = this.dom.bypassSecurityTrustResourceUrl(this.tele_vivo.url);
       console.log("S: ",solicitud);
     })
  }

  home(){
    this.menu.enable(true);
    this.route.navigate(['/home']);
  }
}
