import { Component, OnInit, Input } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.page.html',
  styleUrls: ['./radio.page.scss'],
})
export class RadioPage implements OnInit {
  @Input() texto = "Cargando...";
    datos;
    valor;
    loader = false;
    ruta_estacion;
    estaciones_radio = true;
    estacion_radio = false;
    ruta_radio;
  constructor(private route: Router, private http: HttpClient, private dom: DomSanitizer, 
    private navCtrl: NavController, private menu: MenuController) { 
    }

  ngOnInit() {
    this.menu.enable(true);
    this.getAllRadio();
  }
  getAllRadio(){
    this.loader = true;
    this.menu.enable(true);
    const dir = 'https://hyperiontechlab.com:9011/liveradioapp/getAllStations';
    return this.http.get(dir)
    .subscribe((solicitud) =>{
      this.loader = false;
      this.estaciones_radio = true;
      this.estacion_radio = false;
      this.datos = solicitud;
      this.valor = this.datos.data;
      console.log("Radios: ",solicitud);
    }, (err)=>{
      if(err.error.status === 0){
        console.log("Error al cargar datos");
      }
    })
  }

  Object(val){
    this.menu.enable(true);
    this.loader = true;
    this.estaciones_radio = false;
    this.estacion_radio = true;
    this.ruta_radio = this.dom.bypassSecurityTrustResourceUrl(val.url);
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
    }, 4500);
  }
  home(){
    this.route.navigate(['/home']);
    this.menu.enable(true);
  }

}
