import { Component, OnInit, SecurityContext } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { MenuController, NavController } from '@ionic/angular';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.page.html',
  styleUrls: ['./notificaciones.page.scss'],
})
export class NotificacionesPage implements OnInit {
  ruta_notificacion: any;
  valor_ruta;
  constructor(private route: Router, private http: HttpClient,
    private navCtrl: NavController, private menu: MenuController, private dom: DomSanitizer) {}
    
  ngOnInit() {
    this.menu.close();
    this.menu.enable(true);
    this.valor_ruta = localStorage.getItem('link_not');
    localStorage.removeItem('traer');
    this.ruta_notificacion = this.dom.bypassSecurityTrustResourceUrl(this.valor_ruta);
    //this.valor_ruta = localStorage.getItem('link_not');
    //this.ruta_notificacion = this.dom.bypassSecurityTrustResourceUrl(this.valor_ruta);
    console.log("Local Storage: ",this.valor_ruta);
  }

  home(){
    localStorage.removeItem('link_not');
    this.menu.enable(true);
    this.route.navigate(['/home']);
  }

}
